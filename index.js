import infopack from 'infopack'
import * as combineRecords from './generators/combine-records.js'

let pipelineSteps = [
    // change/edit as you like
    // folderToInfopack.step(),
    combineRecords.step()
];

let pipeline = new infopack.default(pipelineSteps);

pipeline.run();
