import fs from 'node:fs'
import { globby } from 'globby'
import Promise from 'bluebird'
import _ from 'lodash'

export const step = function() {
    return {
        run: function(pipeline) {
            return Promise.mapSeries([
                ['maincategory', 'maincategory', 'mc_id',  'Huvudkategori', 'Värdelista för huvudkategori'],
                ['property',      'property',     'pr_id',  'Egenskap',      'Värdelista för egenskaper'],
                ['schema',        'schema',       'sch_id', 'Schema',        'Värdelista för scheman'],
                ['subcategory',   'subcategory',  'sc_id',  'Underkategori', 'Värdelista för underkategorier'],
                ['system',        'system',       'sys_id', 'System',        'Värdelista för system']
            ], ([folderName, outputName, id, title, description]) => {
                return globby([`input/data/${folderName}/*`])
                    .then(paths => {
                        let list = []
                        paths.forEach(p => {
                            var content = fs.readFileSync(p)
                            list.push(JSON.parse(content.toString()))
                        })
                        let sortedList = _.sortBy(list, [id])
                        return sortedList;
                    })
                    .then(list => {
                        pipeline.toOutput({
                            path: `${outputName}.json`,
                            title: title,
                            description: description,
                            data: JSON.stringify(list, null, 2)
                        })
                    })
            })
        }
    }
}
   